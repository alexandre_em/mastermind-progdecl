(ns mastermind.core
  (:gen-class)
  (:require [mastermind.fonction :as g]
            [mastermind.codedecodeur :as cd]
            [mastermind.codecodeur :as cc]))

(defn -main
  "Lance le jeu Mastermind"
  [& args]
  (let [nb 4]
    (def cptj (atom 1))
    (def cptp (atom 1))
    (while true
      (println "[Menu] :\n Demarrer une partie en tant que ... \n Veuillez entrer le numero\n 1. Decodeur\n 2. Codificateur\n 0. Exit\n Saisir le numero correspondant")
      (def input read-line)
      (case (input)
        "1" (swap! cptj + (cd/joueur-decodeur (g/code-secret nb)))
        "2" (swap! cptp + (cc/joueur-codificateur nb))
        "0" ((if (>= @cptj @cptp)
               (println (str "Vous avez perdu...\nBye for now!\n [Score J-P] : " @cptj " - "@cptp))
               (println (str "Vous avez gagne !\n Bye for now!\n [Score J-P] : " @cptj " - " @cptp))) (System/exit 0))
        ""))))

(let [nb (atom 2)]
  (swap! nb + 9))
