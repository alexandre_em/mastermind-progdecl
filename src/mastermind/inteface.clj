(ns mastermind.inteface 
  (:require [lanterna.screen :as s]
            [mastermind.fonction :as g]))

(defn cote-codeur-graph
  [scr color x y]
  (loop [c color
         cases (- (quot x 2) 12)]
    (if (seq c)
      (do
        (s/put-string scr cases y "|" {:bg :default})
        (s/put-string scr (inc cases) y "     " {:bg (first c)})
        (s/put-string scr (+ 6 cases) y "|" {:bg :default})
        (recur (rest c) (+ cases 6)))
      (s/redraw scr))))

(defn indic-graph
  [scr ind x y]
  (loop [ind ind
         cases (- x (* 3 5))]
    (if (seq ind)
      (do
        (s/put-string scr cases y "|" {:bg :default})
        (s/put-string scr (inc cases) y "  " {:bg (first ind)})
        (s/put-string scr (+ 3 cases) y "|" {:bg :default})
        (recur (rest ind) (+ cases 3)))
      (s/redraw scr))))

(defn affiche-res-graph
  [scr color ind x y]
  (loop [color color
         ind ind
         vres []]
    (if (seq color)
      (recur (rest color) (rest ind) (if (or (= (first ind) :black) (= (first ind) :white))
                                       (conj vres :black)
                                       (conj vres (first color))))
      (do
        (cote-codeur-graph scr vres x y)))))