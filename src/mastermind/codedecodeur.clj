(ns mastermind.codedecodeur
  (:require [mastermind.fonction :as g]
            [mastermind.inteface :as itrf]
            [lanterna.screen :as s]))




(defn joueur-decodeur
  "Lance une partie en tant que decodeur, la partie se finit lorsque le joueur trouve le code ou apres 12 essais"
  [code]
  (do (def scr (s/get-screen))
      (def x (first (s/get-size scr)))
      (def y (- (second (s/get-size scr)) 1))
      (s/start scr)
      (s/put-string scr 0 0 "Cote DECODEUR")
      (s/put-string scr 10 (- (second (s/get-size scr)) 1) "CODE SECRET : ")
      (s/put-string scr (- (quot x 2) 12) 0 "CODE JOUEUR   ")
      (s/put-string scr (- x (* 3 4)) 0 "INDICATION   "))
  (println (str "Entrez les couleurs (red/green/blue/yellow/black/white) taille : " (count code)))
  (loop [vres (g/entree-joueur (count code)) i 0]
    (let [vind2 (g/filtre-indications code vres (g/indications code vres))]
      (println (str "indications (:blanc=:color et :magenta=:good) : \n" vind2))
      (println (str "Entrez les couleurs (red/green/blue/yellow/black/white) taille : " (count code)))
      (do
        (itrf/cote-codeur-graph scr vres x (+ (* 2 i) 1))
        (itrf/indic-graph scr vind2 x (+ (* 2 i) 1))
        (itrf/affiche-res-graph scr vres vind2 x y))
      (if (and (or (g/vcontains vind2 :black) (g/vcontains vind2 :white)) (< i 12))
        (recur (g/entree-joueur (count code)) (inc i))
        (do
          (if (>= i 12)
            (s/put-string scr (quot x 2) (quot y 2) "CODE NON TROUVE")
            (s/put-string scr (quot x 2) (quot y 2) "CODE TROUVE"))
          (s/redraw scr)
          (Thread/sleep 3000)
          (s/stop scr)
          i)))))

      