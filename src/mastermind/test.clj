(ns mastermind.test
  (:use midje.sweet)
  (:require [mastermind.fonction :as g]
            [mastermind.codecodeur :as cc]
            [mastermind.codedecodeur :as cd]))


(fact "Le `code-secret` a l'air aléatoire."
      (> (count (filter true? (map not=
                                   (repeatedly 20 #(g/code-secret 4))
                                   (repeatedly 20 #(g/code-secret 4)))))
         0)
      => true)

(fact "Renvoie les bons indices"
      (g/indications [:red,:green,:yellow] [:red,:yellow,:blue]) => [:magenta :white :black]
      (g/indications [:red,:green,:yellow] [:red,:green,:yellow]) => [:magenta :magenta :magenta]
      (g/indications [:red,:green,:yellow] [:violet,:blue,:blue]) => [:black :black :black])

(fact "Rectifie les indices `v3"
 (g/filtre-indications [:red :red :green :blue]
                     [":green" ":red" ":blue" ":yellow"]
                     [:white :magenta :white :black]) => [:white :magenta :white :black]

 (g/filtre-indications [:red :green :red :blue]
                     [:red :red :blue :red]
                     [:magenta :white :white :white]) => [:magenta :white :white :black]

 (g/filtre-indications [:red :green :yellow :yellow]
                     [:red :yellow :green :green]
                     (g/indications [:red :green :yellow :yellow] [:red :yellow :green :green])) => [:magenta :white :white :black])

(fact "Renvoie le la map des elements ainsi que le frequence du vecteur `v1"
 (g/frequences [:red :red :green :blue]) => {:red 2, :green 1, :blue 1}
 (g/frequences [:black :black :white :magenta]) => {:black 2, :white 1, :magenta 1}
 (g/frequences [12 1 2 3 4 34 3 4 5]) => {12 1, 1 1, 2 1, 3 2, 4 2, 34 1, 5 1})

(fact "Renvoie l'ensemble des couleurs disponible dans le `code"
 (g/filtre-color [:red :red :red :green]) => #{:red :green}
 (g/filtre-color []) => #{}
 (g/filtre-color [:red :blue :violet :magenta]) => #{:red :blue :violet :magenta})

(fact "Renvoie true si le vecteur `v contient l'element `elt"
 (g/vcontains [:good :bad :black] :goud) => false
 (g/vcontains [] :something) => false
 (g/vcontains [:red :green :blue :yellow] :yellow) => true)
