# Introduction to mastermind

Il se présente sous la forme d'un plateau perforé de 12 rangées de quatre trous pouvant accueillir des pions de couleurs.

Le nombre de pions de couleurs différentes est de 6 et les couleurs sont dans la version originale : `jaune`, `bleu`, `rouge`, `vert`, `blanc`, `noir`.

Il y a également des pions `magenta` et `blancs` utilisés pour donner des indications à chaque étape du jeu.